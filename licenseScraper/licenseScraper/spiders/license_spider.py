import scrapy
from licenseScraper.items import LicensescraperItem, LicenseStatusModelItem
from apiapp.models import License

class LicenseSpider(scrapy.Spider):
    name="license"
    licenseNumber = 'F386078204317'

    def start_requests(self):
        url = 'https://mndriveinfo.org/dvsinfo/dv02/DV02.asp?Number=' + licenseNumber
        yield scrapy.Request(url=url, callback=self.parse)
        
    def parse(self, response):
        status = response.xpath('//body/form/div/table[1]/tr[2]/td[4]/text()').extract()
        expirationDate = response.xpath('//body/form/div/table[2]/tr[2]/td[4]/text()').extract()
        number = response.xpath('//body/form/div/table[1]/tr[2]/td[1]/text()').extract()
        dlclass = response.xpath('//body/form/div/table[1]/tr[2]/td[2]/text()').extract()
        issued = response.xpath('//body/form/div/table[2]/tr[2]/td[3]/text()').extract()
      
        item = LicensescraperItem()
        licenseStatus = LicenseStatusModelItem()
         
        item['DL_Expiration'] = expirationDate[0].strip()
        item['DL_Status'] = status[0]
        item['DL_Number'] = number[0]
        item['DL_Class'] = dlclass[0]
        item['DL_Issued'] = issued[0]
        
        
        license = License()
        license = License.objects.get(number=item['DL_Number'])
        licenseStatus['status'] = 1
        licenseStatus['license'] = license
        licenseStatus.save()
        #item['DLExpiration'] = info.selector.xpath('./td[2]/text()').extract()
        return item