# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from apiapp.models import LicenseStatusModel, License

class LicensescraperItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    DL_Expiration = scrapy.Field()
    DL_Number = scrapy.Field()
    DL_Class = scrapy.Field()
    DL_Status = scrapy.Field()
    DL_Issued = scrapy.Field()
    pass

# Importing django items into scrapy project. 
# This allows us to use django models in scrapy project. 
from scrapy_djangoitem import DjangoItem
class LicenseStatusModelItem(DjangoItem):
    django_model = LicenseStatusModel
    
class LicenseItem(DjangoItem):
    django_model = License