#!/bin/bash

HOME_DIR="/home/"$USER
PROJ_DIR=$HOME_DIR/openhome
PROJ_NAME=openhomeproject
HOST_IP=$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
SOCKET_DIR='/tmp'
SOCKET_NAME='uwsgi.sock'
DATABASE_NAME='openhomedb'
DATABASE_USERNAME='openhomeadmin'
DATABASE_PASSWORD='home1234'

# Update the system
sudo apt-get update -y
# Upgrade the system
sudo apt-get upgrade -y
# Install Virt Environment
sudo apt-get install virtualenv -y
# Install vim
sudo apt-get install vim -y

# create a virtual environment
virtualenv openhome_env

sudo apt-get install python-dev -y

# Install scrapy-djangoitem
sudo pip install scrapy-djangoitem

sudo apt-get install postgresql -y

sudo -u postgres -H -- psql -c "create user $DATABASE_USERNAME with password '$DATABASE_PASSWORD';"
sudo -u postgres -H -- psql -c "create database $DATABASE_NAME;"
sudo -u postgres -H -- psql -c "alter role $DATABASE_USERNAME set client_encoding to 'utf8';"
sudo -u postgres -H -- psql -c "alter role $DATABASE_USERNAME set default_transaction_isolation to 'read committed';"
sudo -u postgres -H -- psql -c "alter role DATABASE_USERNAME set timezone to 'UTC';"

sudo -u postgres -H -- psql -c "grant all privileges on database $DATABASE_NAME to $DATABASE_USERNAME;"

sudo apt-get install nginx -y
sudo /etc/init.d/nginx start

# activate virtual environment. 
# All required python frameworks will be installed in virtual environment
source openhome_env/bin/activate
# Install uWSGI
sudo pip install uwsgi
pip install django
pip install djangorestframework
pip install markdown
pip install django-filter
sudo apt-get install libpq-dev -y 
pip install psycopg2
pip install paho-mqtt

cat > $PROJ_DIR/${PROJ_NAME}_nginx.conf <<EOF
# openhomeproject_nginx.conf

# the upstream component nginx needs to connect to
upstream django {
    server unix:$SOCKET_DIR/$SOCKET_NAME;
}

# configuration of the server
server {
    # the port your site will be served on
    listen      8000;
    # the domain name it will serve for
    server_name $HOST_IP;  
    charset     utf-8;

    # max upload size
    client_max_body_size 75M;   # adjust to taste

    # Django media
    location /media  {
        alias $PROJ_DIR/$PROJ_NAME/media;  # your Django project's media files - amend as required
    }

    location /static {
        alias $PROJ_DIR/$PROJ_NAME/static; # your Django project's static files - amend as required
    }

    # Finally, send all non-media requests to the Django server.
    location / {
	uwsgi_pass  django;
        include     $PROJ_DIR/$PROJ_NAME/uwsgi_params;  # the uwsgi_params file you installed
    }
}
EOF

sudo ln -s $PROJ_DIR/${PROJ_NAME}_nginx.conf /etc/nginx/sites-enabled/

python $PROJ_DIR/$PROJ_NAME/manage.py collectstatic --noinput

cat > $PROJ_NAME/${PROJ_NAME}_uwsgi.ini <<EOF
[uwsgi]
# --------------------
# Settings:
# key = value
# Comments >> #
# --------------------

# socket = [addr:port]
socket = $SOCKET_DIR/$SOCKET_NAME

chmod-socket = 666

# Base application directory
# chdir = /full/path
chdir = $PROJ_DIR/$PROJ_NAME 

# WSGI module and callable
# module = [wsgi_module_name]:[application_callable_name]
module = openhomeproject.wsgi:application

#master = [master process (true or false)]
master = true

#processes = [number of processes]
processes = 5
EOF

sudo sh -c "echo '
#!/bin/bash
uwsgi --master --die-on-term --emperor $PROJ_DIR/$PROJ_NAME/${PROJ_NAME}_uwsgi.ini 
' > $PROJ_DIR/uwsgi.sh"

sudo chmod u+x $PROJ_DIR/uwsgi.sh

sudo sh -c "echo '[Unit] 
Description=openhome uWSGI
After=syslog.target

[Service]
ExecStart=/usr/local/bin/uwsgi --ini $PROJ_DIR/$PROJ_NAME/${PROJ_NAME}_uwsgi.ini
RuntimeDirectory=uwsgi
Restart=always
KillSignal=SIGQUIT
Type=notify
StandardError=syslog
NotifyAccess=all

[Install]
WantedBy=multi-user.target' > /lib/systemd/system/uwsgi.service"
sudo chmod 777 /lib/systemd/system/uwsgi.service
sudo systemctl daemon-reload
sudo systemctl start uwsgi.service
sudo systemctl status uwsgi.service
