# import serial
#import sqlite3
# from sensordata import SensorData
# from datetime import datetime
import time
from random import randint

# Instantiate serial communication 
# ser = serial.Serial('/dev/tty.usbmodem1421', 9600)
#ser.close()
#ser.open()
#ser.flushInput()
#ser.flushOutput()                   

# Instantiate database
# conn = sqlite3.connect('gatewaydata.db')
#c = conn.cursor()
#c.execute('''CREATE TABLE IF NOT EXISTS sensordata (nodeid INT, uptime UNSIGNED BIT INT, temp REAL, humidity REAL)''')

#c.execute("INSERT INTO sensordata VALUES(1, 1000, 54.34, 34.23)")
#conn.commit()
#conn.close()

# sensorData = SensorData()
memoryFree = 0

def handleNode():
    node = ser.readline()
    sensorData.nodeId = node
    print(node)
    print("-----")

def handleTemp():
    temp = ser.readline()
    sensorData.temp = temp
    print(temp)
    print("-----")

def handleHumidity():
    humidity = ser.readline()
    sensorData.humidity = humidity
    print(humidity)
    print("-----")

def handleUptime():
    uptime = ser.readline()
    sensorData.uptime = uptime
    print(uptime)
    print("----")

def startDataGather():
    print("New Sensor Data")
    sensorData = SensorData()

def handleMemory():
    sensorData.freeMemory = ser.readline()
    print(sensorData.freeMemory)

def endDataGather():
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS sensordata (nodeid INT, uptime UNSIGNED BIT INT, temp REAL, humidity REAL)''')

    c.execute('INSERT INTO sensordata VALUES(?, ?, ?, ?, ?, ?)',[sensorData.nodeId, sensorData.uptime, sensorData.temp, sensorData.humidity, datetime.now(), sensorData.freeMemory])
    conn.commit()

    print("Send data to db")

commandOptions = {
    "Z" : startDataGather,
    "N" : handleNode,
    "T" : handleTemp,
    "H" : handleHumidity,
    "M" : handleMemory,
    "U" : handleUptime,            
    "X" : endDataGather,
}
 
import requests
sensorValue = 1.00
while True:
    #command = ser.read(1)
    #print(command)
    # if command in commandOptions:
    #     commandOptions[command]()
    time.sleep(2)
    data = {"sensor_id":"1", "value":sensorValue}
    response = requests.post('http://localhost:8000/api/v1/post_data_point/', json=data)
    print response.status_code
    sensorValue = randint(0, 9) 


