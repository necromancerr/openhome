from django.conf.urls import url

import views

urlpatterns = [
    url(r'^data_points/', views.test_api),
    url(r'^post_data_point/', views.post_data_point),
    url(r'^list/', views.node_list),
    url(r'^insertLicenseStatus', views.insertLicenseStatus)
]


    