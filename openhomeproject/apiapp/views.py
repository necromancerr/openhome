from django.shortcuts import render
from rest_framework.decorators import api_view
from nodeapp.models import SensorData, SensorDataSerializer, Node, NodeSerializer
from models import LicenseStatusSerializer
from rest_framework.response import Response
from rest_framework import status 
from django.http import HttpResponse
import json
from datetime import datetime
import time



# Create your views here.

@api_view(['GET'])
def test_api(request):
    if (request.method == 'GET'):
        dataPoints = SensorData.objects.all().order_by('-id')[:10]
        row = {"time_stamp": [], "value":[]}
        for data in dataPoints:
            timeStamp = data.time_stamp.strftime("%Y-%m-%d %H:%M:%S")
            value = str(data.value)
            row["time_stamp"].append(timeStamp)
            row["value"].append(value)
        return HttpResponse(json.dumps(row), content_type="application/json") 


@api_view(['POST'])
def post_data_point(request):
    if (request.method == 'POST'):
        data = request.data
        serializerData = SensorDataSerializer(data=data)
        if serializerData.is_valid():
            serializerData.save()
            return Response(serializerData.data, status=status.HTTP_201_CREATED)
        return Response(serializerData.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET'])
def node_list(request):
    if (request.method == 'GET'):
        nodes = Node.objects.all()
        serializer = NodeSerializer(nodes, many=True)
        return Response(serializer.data)

def test_api(request):
    if (request.method == 'GET'):
        dataPoints = SensorData.objects.all().order_by('-id')[:10]
        row = {"time_stamp": [], "value":[]}
        for data in dataPoints:
            timeStamp = data.time_stamp.strftime("%Y-%m-%d %H:%M:%S")
            value = str(data.value)
            row["time_stamp"].append(timeStamp)
            row["value"].append(value)
        return HttpResponse(json.dumps(row), content_type="application/json") 
        
@api_view(['POST']) 
def insertLicenseStatus(request):
    if (request.method == 'POST'):
        licenseData = request.data
        serializerData = LicenseStatusSerializer(data=licenseData)
        if serializerData.is_valid():
            serializerData.save()
            return Response(serializerData.data, status=status.HTTP_201_CREATED)
        return Response(serializerData.errors, status=status.HTTP_400_BAD_REQUEST)