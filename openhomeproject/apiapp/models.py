from __future__ import unicode_literals
from rest_framework import serializers
from django.db import models

# Create your models here.
class License(models.Model):
    number = models.CharField(max_length=13)
    
    def __str__(self):
        return self.number
        

class LicenseStatusModel(models.Model):
    license = models.ForeignKey(License, default=1, related_name='validity', on_delete=models.DO_NOTHING)
    status = models.BooleanField()
    timeStamp = models.DateTimeField(editable=False, auto_now=True)
    def __str__(self):
        return self.status
    
    
class LicenseStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = LicenseStatusModel
        field = ('id', 'license', 'status')
        
        
