"""openhomeproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from openhomeapp import views
from rest_framework import routers
from nodeapp import views as node_view
from django.conf import settings
from django.conf.urls.static import static

router = routers.DefaultRouter()

API_ROUTE = 'api/'
API_VERSION = "v1/"

urlpatterns = [
    # url(r'^$', views.openhome, name="homepage"),
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, name="login"),

    url(r'^api_auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^' + API_ROUTE + API_VERSION, include(router.urls)),
    
    url(r'^' + API_ROUTE + API_VERSION, include("apiapp.urls")),
    url(r'^nodes/', include('nodeapp.urls')),

    url(r'^openhome/', include('openhomeapp.urls'),)
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
