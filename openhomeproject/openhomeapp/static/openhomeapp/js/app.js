'use strict';

var hostname = "http://localhost:8000";

angular.module("openhomeapp", ['ngMaterial', 'ui.router']);
angular.module('openhomeapp').config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
});

// angular.module('openhomeapp').config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
//     $routeProvider
//     .when('/dashboard', {
//         templateUrl: 'dashboard.html',
//         controller: 'HomeController'
//     })
//     .when('/nodes', {
//         templateUrl: 'nodes.html',
//         controller: 'NodesController'
//     }) 
//     //.otherwise({redirectTo: '/'});

// }]);

angular.module('openhomeapp').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    //$urlRouterProvider.otherwise('/');

    $stateProvider.
        state('dashboard', {
            url: '/dashboard',
            views: {
                "openhomeview": {
                    templateUrl: 'dashboard.html',
                    controller: 'HomeController'
                }
            }
        }).
        state('nodes', {
            url: '/nodes',
            views: {
                "openhomeview": {
                    templateUrl: 'nodes.html',
                    controller: 'NodesController'
                }
            }
        }).
        state('node', {
            parent: 'nodes',
            url: '/node',
            views: {
                "nodedetailview": {
                    templateUrl: 'node.html'
                }
            } 
        })
}]);