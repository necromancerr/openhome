var app = angular.module('openhomeapp');
app.controller('NodesController', ['$scope', '$http', function($scope, $http) {
    console.log("Loading NodesController");

    $http.get(hostname+'/api/v1/list').
        then(function(response) {
            $scope.nodes = response.data;
        }
    );

    // $scope.redirectToNode = function() {
    //     $state.go('node');
    // }

}]);