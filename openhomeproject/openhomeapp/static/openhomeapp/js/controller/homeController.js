    var app = angular.module('openhomeapp');
    app.controller('HomeController', ['$scope', '$mdSidenav', '$http', function($scope, $mdSidenav, $http) {
        $scope.name = "testing please work";
        console.log("inside homecontroller");
        $scope.toggleMenu = function() {
            $mdSidenav('left').toggle();
        }
    
        // Uncomment if you want to get weather data from server. Don't forget to make necessary changes on the UI to actually see the changes'
        // $http.get('https://api.forecast.io/forecast/d956445ea12f0780b0e0b98ecfcece66/44.948967,-93.384347')
        //     .then(function(response) {
        //         console.log(response.data);
        //     });
       
        var chart = c3.generate({
            bindto: '#chart',
            size: {
                height: 150
            }, 
            data: {
                url: 'http://localhost:8000/api/v1/data_points/',
                x: 'time_stamp',
                xFormat: '%Y-%m-%d %H:%M:%S', // 'xFormat' can be used as custom format of 'x'
                value: 'value',
                mimeType: 'json'
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%M:%S'
                    }
                }
            }
        });
    }]);