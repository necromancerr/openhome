from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.openhome),
    url(r'^dashboard.html/$', views.dashboard),
    url(r'^nodes.html/$', views.nodes),
    url(r'^node.html/$', views.nodeDetail)
]


    