from django.shortcuts import render, redirect, reverse
from django.conf import settings
import requests

# Create your views here.
def openhome(request):
    # if request.user.is_authenticated():
        return render(request, 'openhome.html', {'STATIC_URL':settings.STATIC_URL})
    # else:
        # return redirect(reverse('login'))

def dashboard(request):
    weatherResponseData = requests.get("https://api.forecast.io/forecast/d956445ea12f0780b0e0b98ecfcece66/44.948967,-93.384347")
    temperatureData = {"currentTemp":"-1"}
    if weatherResponseData.status_code == 200:
        temperatureData["currentTemp"] = weatherResponseData.json()["currently"]["temperature"]
    

    return render(request, 'dashboard.html', {'temperatureData': temperatureData})

def nodes(request):
    return render(request, 'nodes.html')

def nodeDetail(request):
    return render(request, 'nodedetail.html')


def login(request):
    return render(request, 'registration/login.html')
