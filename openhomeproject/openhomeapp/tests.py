from django.test import TestCase
from django.test import Client
import views

# Create your tests here.
class TestMethodTests(TestCase):
    def setUp(self):
        client = Client()

    def test_openhome(self):
        response = self.client.get('/openhome/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, views.openhome)
        


