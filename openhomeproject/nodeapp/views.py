from django.shortcuts import render
from nodeapp.models import Node, NodeSerializer
from django.views.generic.list import ListView
from rest_framework.decorators import api_view
from django.http import HttpResponse
from rest_framework.response import Response  
# Create your views here.

class NodeListView(ListView):
    model = Node
    template_name="node-list.html"

    def get_context_data(self, **kwargs):
        context = super(NodeListView, self).get_context_data(**kwargs)
        context['node_list'] = Node.objects.all()
        return context
        
        
 