# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-04 16:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('nodeapp', '0004_auto_20160903_0123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sensordata',
            name='sensor_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='data_points', to='nodeapp.Sensor'),
        ),
    ]
