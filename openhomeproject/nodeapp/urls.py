from django.conf.urls import url
from nodeapp.views import NodeListView
import views

urlpatterns = [
    url(r'^list/', NodeListView.as_view()),
]


    