from django.contrib import admin

# Register your models here.
from .models import Node, Sensor

admin.site.register(Node)