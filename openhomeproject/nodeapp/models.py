from __future__ import unicode_literals
from decimal import Decimal
from rest_framework import serializers

from django.db import models

# Create your models here.

"""Class that defines a node. 
Node is a unit that will be placed on the desired location.
A Node can have multiple Sensors. 
"""
class Node(models.Model):
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=30)
    #sensors = models.ForeignKey(Sensor, on_delete=models.DO_NOTHING, related_)

    def __str__(self):
        return self.name 

"""Class that defines a Sensor.

A Sensor can only be associated with a single Node. 
"""
class Sensor(models.Model):
    name = models.CharField(max_length=30)
    unit = models.CharField(max_length=5)
    node = models.ForeignKey(Node, default=1, related_name='sensors', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name

"""Class that defines a Sensor's Data.

All data added will be automatically timestamped"""
class SensorData(models.Model):
    sensor_id       = models.ForeignKey(Sensor, on_delete=models.DO_NOTHING, related_name='data_points') 
    value           = models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=4)
    time_stamp      = models.DateTimeField(editable=False, auto_now=True)

"""Serarilizer for SensorData
Used for rest api
"""
class SensorDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = SensorData
        fields = ('id', 'sensor_id', 'value', 'time_stamp')

"""Serializer for Sensors. 
Used for rest api
"""
class SensorSerializer(serializers.ModelSerializer):
    data_points = SensorDataSerializer(many=True, read_only=True) 
    
    class Meta:
        model=Sensor
        field = ('name', 'unit', 'data_points')

"""Serializer for Node.
Used for rest api
"""
class NodeSerializer(serializers.ModelSerializer):
    sensors = SensorSerializer(many=True, read_only=True)
    
    class Meta:
        model = Node
        field = ('id', 'name', 'sensors', 'location')