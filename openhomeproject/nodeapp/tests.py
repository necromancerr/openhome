from django.test import TestCase
from nodeapp.models import Node, Sensor, SensorData
from decimal import Decimal

def make_node(name, location=''):
    return Node.objects.create(name=name, location=location)

def make_sensor(name, unit, node):
    return Sensor.objects.create(name=name, unit=unit, node=node)

def make_sensorData(sensor_id, value):
    return SensorData.objects.create(sensor_id = sensor_id, value=value) 


# Create your tests here.
class NodeTest(TestCase):
    def setUp(self):
        self.node = make_node('NODE01', 'LivingRoom') 
        self.sensor = make_sensor('SENSOR01', 'V', self.node)
        self.sensorData1 = make_sensorData(self.sensor, 88.88)
        self.sensorData2 = make_sensorData(self.sensor, 79.20)
        self.sensorData3 = make_sensorData(self.sensor, 11.88)

    def test_node(self):
        node = self.node
        self.assertEqual(node.name, "NODE01")
        self.assertEqual(node.location, "LivingRoom")
        self.assertEqual(node.sensors.first(), self.sensor)

    def test_sensor(self):
        sensor = self.sensor
        self.assertEqual(sensor.name, 'SENSOR01')
        self.assertEqual(sensor.node, self.node)

    def test_sensor_data(self):
        #self.assertEqual(self.sensorData1.value, 88.88)
        sensorD = self.sensor.data_points.first()
        self.assertEqual(sensorD.sensor_id, self.sensor)
        self.assertEqual(self.sensor.data_points.all().count(), 3) 